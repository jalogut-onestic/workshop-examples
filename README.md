# <ProjectName> E-Shop

## Installation

0. Clone repository and run composer install

	```
	git clone -b develop <git-url> <project-name>
	cd <project-name>
	composer install
	```

## Frontend

* [Frontend Documentation](docs/development/frontend.md)
